<center><h1>Joshua Morgan</h1> </center>


<center>Washington, DC 20003 (202) 460-6598</center>

Email [joshualawrencemorgan@gmail.com](mailto:joshualawrencemorgan@gmail.com) Web [https://joshualmorgan.com/](https://joshualawrencemorgan.gitlab.io/post) Clearance TS/SCI

EDUCATION
============

B.S., Criminal Justice, The Citadel, the Military College of South Carolina, Charleston, SC. 2016. 

Tool Developer Qualification Course, University of Maryland Baltimore County, Columbia, MD. 2019.

PROFESSIONAL
============

-   Reverse engineered embedded system equipment to understand how the
    system hardware, software, and firmware operate together

-   Developed harnesses to run and fuzz cross architecture networked
    applications running on embedded Linux firmware (primarily MIPS and
    ARM) without source code

-   Used disassemblers, debuggers, and other assembly-level tools to
    perform analysis of compiled binary files (usually x86/64,
    occasionally ARM or other architectures)

-   Developed an extensive knowledge of C, python, assembly language,
    scripting and other programming languages

-   Provided written summary of the work as well as proofs of concept to
    present to the senior leadership

-   Supervised and mentored junior researchers; lead detachment training
    session covering secure coding practices, user after free
    vulnerabilities, and other vulnerability topics

**Digital Network Exploitation Analyst, 101 CMT -** U.S. Army CyberSep
2018 - Sep 2019

-   Provided crucial network intelligence and analysis to support cyber
    network operations

-   Conducted analysis of metadata, targets, and target research to
    further mission package development

-   Performed surveillance and reconnaissance actions on specified
    systems and networks to identify new methods of entry and
    exploitation

-   Identified target communications within the global network to
    determine patterns of life and PACE plans

-   Produced multiple intelligence reports for the Intelligence
    Community that turned raw information into a readable, actionable
    media to conduct cyber operations and support mission commanders

SKILLS
======

**Certifications -**Certified Ethical Hacker, CompTIA Security`+` & Network`+`, Basic Army Developer

**Software -** Ghidra, afl, Mayhem, Docker, & GDB

**Architectures -** MIPS, ARM, amd64

**Languages -** C, Python, LaTeX, & Bash scripting
