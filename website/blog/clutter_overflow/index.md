# PicoCTF: Clutter Overflow

[Link to challenge](https://play.picoctf.org/practice/challenge/216)

## The Challenge

The next challenge is titled "Clutter Overflow" and also comes with the tag - binary exploitation. The description, "Clutter, clutter everywhere and not a byte to use", and the title indicate that we might be able to overflow a buffer and control some bytes. Attached is a file called `chall.c`, an executable called `chall`, and the command line instruction *nc mercury.picoctf.net 17612*.

## The Solve

### PWNINIT
As we learned last time, a good first step is pwninit which sets up [solve.py](solve.py) and patches the binary with the correct loader and linker. This challenge doesn't actually require all of that, `chall` works for me immediately, but it's still a nice tool. 

### Quick analysis
A quick check at the C file shows some cool ascii art and a simple main program with a system call to *cat flag.txt*.

```C
int main(void)
{
  long code = 0;
  char clutter[SIZE];

  setbuf(stdout, NULL);
  setbuf(stdin, NULL);
  setbuf(stderr, NULL);
 	
  puts(HEADER); 
  puts("My room is so cluttered...");
  puts("What do you see?");

  gets(clutter);


  if (code == GOAL) {
    printf("code == 0x%llx: how did that happen??\n", GOAL);
    puts("take a flag for your troubles");
    system("cat flag.txt");
  } else {
    printf("code == 0x%llx\n", code);
    printf("code != 0x%llx :(\n", GOAL);
  }
```

### Testing

Running both the provided `chall` binary and our pwninit patched version results in behavior you'd expect from a binary made from that C file (*compiling the c file yourself could mess up the intended vulnerability*). The program puts `what do you see?` and then reads from stdin.

![chall_a](img/chall_a.png)

### Getting the flag
So the task seems pretty simple fill the buffer called `clutter` and overflow it so that `code` is equal to `0xdeadbeef`. Unfortunately, I was unfamiliar with pwntool's cyclic functionality so I just hand jammed it until I got the value I wanted.
![stack](img/chall_stack.png)

Hand jamming wasnt too hard, we already know that the size of the buffer `SIZE` was 0x100.

A few lines into my python function and I was able to grab the local and then remote flag.

### Conclusion
This one was pretty easy all things considered. There was a slight detour because I compiled my own chall from the c file with ended up slighly [different](img/mygcc_clutter.png) than [the provided one](img/theirgcc_clutter.png). Otherwise, a fun one to take on and have under my belt.