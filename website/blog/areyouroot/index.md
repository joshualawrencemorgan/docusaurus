# PicoCTF: Are You Root

## The Challenge

The next challenge is titled "Are You Root"

## The Solve

### Quick analysis

A quick look over of the auth.c file that we are handily provided shows something of a menu with some small functionality attached. Menus are fun to write code for and I have some experience in this area. I also have some (slightly detrimental, but thankfully past) experience in uninitialized variables that are part of a structure. These two things, to me, point to some sort of heap manipulation vulnerability.


### PWNINIT

pwninit is a tool for automating starting binary exploit challenges that uses patchelf to fix binaries. It allows us to send and recieve lines inside of a python script.

### Ghidra
Ghidra almsot always proves useful in these problems, if only to provide a different view of the binary. 

![Building the user structure in Ghidra](img/ghidrastruct.png)

In this CTF, it forced me to take a look at user structure and get a clear understanding of the members and offsets.

### Testing
So let's test my hypothesis about the uninitialized variables. Running the binary with inside our pwntools script and attaching to the process with gdb/gef we are able to stop the program and look at the heap chunks. I chose to break on the puts that prings out "Enter your command". With this we can see what the program looks like before a user is created.

![before user creation](img/before_user.png)

Here we see quite a few chunks have been created. We could go through and figure which chunk is which variable but I'm more eager to go through and see what happens with our user creation.

![after user creation](img/post_user_creation.png)

I specifically entered a user name with 0x20 bytes because I saw that that was the size of our user structure. Above we can see that our string got malloced at addr=0xb81290 and our user structure got addr=0xb80260. Notice that the user level on our user structure is blank. Im going to try to set it to 3 and see where that data is stored; according to our Ghidra studies it should be right after the address pointer.

![heap of unintialized](img/set-auth.png)

Here we see that our structure diagram was correct. 8 bytes after char * name we see the number for level set to 3. Here is where the size of our username comes in. 

![it's over 9000!](img/level9000.png)

Tada! All of a sudden our unintialized level variable is now some random number (or more specifically the numberic value of 0x61616161.) How did this happen? Well from my experience in Windows heap exploitation I know that heap managers like to "cheat". If they have a recently freed chunk that matches the size of a new chunk being malloc they like to reuse the chunk. These design decisions help optimize memory management and would be completely fine if the author of this program had not left level unintialized. This is why I created a user name buf the same size as the structure. Hoping for the the heap manager to pick up our old chunk where the data field at user->level would be our old data.

### Getting the flag

This is great but instead of some random number we need it to be exactly 5. So instead of a 16 byte username full of 'a's let's try to get the number 5 in there. Because of the work with Ghidra and our set-auth experiment we already know exactly what our username needs to be to do this. Crafting our [solve.py](solve.py) file to login as a user with the name 'aaaaaaaa\0x05\x00\x00\x00', reseting to free those chunks and creating a new user (name doesnt matter at all) will result a new user with auth level 5. All thats left is to give the command get-flag.

### Conclusion
This was a fun foray into manipulating the heap, unitialized variables and use-after-free. This is a pretty simple example of a sometimes pretty complex vulnerability. Much harder to account for then things like a buffer overflow, UAF vulnerabilities seem to be more common. It was fun to take a crack at this challenge.