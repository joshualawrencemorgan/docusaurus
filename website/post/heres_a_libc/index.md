# PicoCTF: Here's a LIBC

[Link to challenge](https://play.picoctf.org/practice/challenge/179)

## The Challenge

"Here's a LIBC" is another challenge on picoCTF authored by MADSTACKS (*which gives us our first clue*). The description isn't very helpful but the challenge comes with a `vuln` binary, a libc, a makefile and the `nc` command to *mercury.picoctf.net 42072*.

## The Solve

### PWNINIT
First, I checked the binary just to see if I could learn anything and unsurprisingly it crashed. I immediately went to *pwninit* which was incredibly helpful during the `Cache Me Outside`. PWNINIT created [solve.py](solve.py) and patched the binary `vuln` to `vuln_patched`. 

### Quick analysis
We can now run the binary! Spinning it up reveals that it's some sort of echo-ing binary. A look in ghidra reveals the same. 
![ghidra_main](img/ghidra_main.png)
So the binary gets set up and then alternates Uppercase on "welcome to my echo server!". From then on out the binary stays inside of `do_stuff`.
![do_stuff](img/ghidra_do_stuff.png)
Here it's pretty easy to see the mistake in this program. The binary sets up a buffer and then uses scanf to read until a newline. So very much like the last challenge `Clutter Overflow` we've found a buffer overflow but this challenge requires more than that because the function simply returns after changing the case on the first 100 characters of the provided string and printing it out.

### Testing the overflow
So let's test that theory, I threw a long string of a's at the binary and sure enough it crashes. I popped open gdb and saw that we were overwriting registers and the return address. This is exciting, I have spent a lot of time practicing finding vulnerabilities so spotting them has become easier for me. I hadn't spent a lot of time actually explioting them so there was a lot for me to learn here.

### Return Oriented Programming
ROP or Return Oriented Programming is an expliotation concept where you manipulate the return address to make the program do something. Normally this means chaining together tiny bits of the binary's assembly to circumvent any defense and pop a shell. I had a feeling we wanted to end up with a system call that either popped a shell or just straight up called cat on flag.txt. 

Once again the pwntools came in clutch. Admittedly, I understood what we needed to do but needed help learning the library. [This write up](https://github.com/Dvd848/CTFs/blob/master/2021_picoCTF/Heres_a_LIBC.md) is fantastic and pretty concise. But essentially, we want to know where the libc function *system* is at in our binary. We don't know where that call is but we do know where *puts* is and where the GOT is. So we can use the overflow to have program call puts with our provided argument which will be the address in the GOT where the runtime address of puts is. We obviously do not want the program do end here as we have the next return address call do_stuff again allowing us to exploit the overflow a second time. The rop chain looks like this:
```bash
[*] First ROP Chain:
    0x0000:         0x400913 pop rdi; ret
    0x0008:         0x601018 [arg0] rdi = got.puts
    0x0010:         0x400540 puts
    0x0018:         0x4006d8 do_stuff()
```
### Getting the flag
After our first rop chain leaks the offset of libc and returns us to the do_stuff loop we can send another payload. Now that we know the offset of libc we can use the pwntools library function called libc to calculate the offset of the function system. We can also use pwntools to search the libc for the string "/bin/sh".
```python
    rop.call(libc.symbols["system"], [next(libc.search(b"/bin/sh"))])
```
Note that there is a dummy call to puts to align the stack in [solve.py](solve.py).
Sending this payload delivers us a shell that we can then execute `cat flag.txt` to get the flag.

### Conclusion

This was a fun CTF! It was a good next step from Clutter Overflow that required some next level skills. It again pushed me to learn more of the pwn library which has proved to be worth it's weight in gold. It was a fun first step into building ROP chains and I hope for more CTFs like this going forward.