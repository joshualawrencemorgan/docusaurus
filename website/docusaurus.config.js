// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Joshua Morgan',
  tagline: 'Code, Hacks, and more',
  url: 'https://joshualmorgan.com',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/digdug.ico',
  organizationName: 'joshualawrencemorgan', // Usually your GitHub org/user name.
  projectName: 'joshualawrencemorgan.gitlab.io', // Usually your repo name.
  trailingSlash: false,

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl: 'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Joshua Morgan',
        logo: {
          alt: 'My Site Logo',
          src: 'img/digdug.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'about',
            position: 'left',
            label: 'About',
          },
          {to: '/blog', label: 'Post', position: 'left'},
          {
            href: 'https://gitlab.com/joshualawrencemorgan',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'About',
                to: '/docs/about',
              },
            ],
          },
          {
            title: 'Contact',
            items: [
              {
                label: 'Mail',
                href: 'mailto: joshualawrencemorgan@gmail.com',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Post',
                to: '/blog',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/joshualawrencemorgan',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Joshua Morgan`,
      },
      prism: {
        theme: darkCodeTheme,
        darkTheme: lightCodeTheme,
      },
    }),
};

module.exports = config;
