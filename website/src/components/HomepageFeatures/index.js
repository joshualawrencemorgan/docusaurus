import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Code',
    Svg: require('@site/static/img/left_digdug.svg').default,
    description: (
      <>
        Take a look at my previous projects 
      </>
    ),
  },
  {
    title: 'CTFs',
    Svg: require('@site/static/img/down_digdug.svg').default,
    description: (
      <>
        Read my write ups in the <code>blog</code> directory.
      </>
    ),
  },
  {
    title: 'About',
    Svg: require('@site/static/img/digdug.svg').default,
    description: (
      <>
        Check my bio and resume in the about folder
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
